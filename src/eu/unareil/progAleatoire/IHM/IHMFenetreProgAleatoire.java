package eu.unareil.progAleatoire.IHM;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import eu.unareil.progAleatoire.bll.ProgAleatoire;
import eu.unareil.progAleatoire.bo.Stagiaire;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.scene.input.MouseEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.control.TextField;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.Alert;
import javafx.scene.layout.GridPane;
import javafx.animation.PauseTransition;
import javafx.util.Duration;
import javafx.application.Platform;

public class IHMFenetreProgAleatoire extends Application {
private List <Text> lesNoms=new ArrayList<>();
List<CheckBox> colCb = new ArrayList<>();
private ProgAleatoire pr;
private Stagiaire stTemp;
private Alert dialog;
private Text compt;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
launch();
	}

	@Override
	public void start(Stage fen) throws Exception {
		// TODO Auto-generated method stub
		  Stagiaire stagiaires[]= new Stagiaire[9];
//		  stagiaires[0]=new Stagiaire("Angélique","GEMARIN");
//		  stagiaires[1]=new Stagiaire("Mickaël","LECLERC");
//		  stagiaires[2]=new Stagiaire("Noémie","LECLEVE");
//		  stagiaires[3]=new Stagiaire("Andréa","LUTTON");
//		  stagiaires[4]=new Stagiaire("Yann","MAILLOT");
//		  stagiaires[5]=new Stagiaire("Ronan","PENDU");
//		  stagiaires[6]=new Stagiaire("Islem","SLAMA");
//		  stagiaires[7]=new Stagiaire("Loubna","TANI");
//		  stagiaires[8]=new Stagiaire("Florent","VESLIN");
//		  stagiaires[9]=new Stagiaire("Alexandre","VIGNERON");
		  stagiaires[0]=new Stagiaire("Christophe","GARCIA");
		  stagiaires[1]=new Stagiaire("Philippe","JUBLOT");
		  stagiaires[2]=new Stagiaire("Chloé","LE NAIN-SEDJAME");
		  stagiaires[3]=new Stagiaire("Nelson","MAGALHAES");
		  stagiaires[4]=new Stagiaire("Robin","MOREAU");
		  stagiaires[5]=new Stagiaire("Clément","POLLET");
		  stagiaires[6]=new Stagiaire("Maxime","PONTOIS");
		  stagiaires[7]=new Stagiaire("Nicolas","PROU");
		  stagiaires[8]=new Stagiaire("Camille","SECULA");
		 pr = ProgAleatoire.getInstance();
		pr.ajouteTous(stagiaires);
	
		  /* affiche une boite de dialogue
		  TextInputDialog inDialog = new TextInputDialog();
		  inDialog.setTitle("Nombre de tour");
		  inDialog.setHeaderText("Combien de tours");
		  inDialog.setContentText("Nombre de tours :");
		  java.util.Optional<String> textIn = inDialog.showAndWait();
		  //--- Get response value (traditional way)
		  if (textIn.isPresent()) {
		  System.out.println("Login name = " + textIn.get());
		  }
		  */
		  initComposants(fen);
		fen.show();
		
	}
	public void initComposants(Stage fen)
	{

		Text lab = null;
		CheckBox cb =null;


		for(Stagiaire st : pr.afficheTous()) {
			lab = new Text(st.getPrenom()+" "+st.getNom());
			lab.setFont(Font.font ("Verdana", 20));
			lesNoms.add(lab);
			cb= new CheckBox();
			cb.setSelected(true);
			colCb.add(cb);
		}
		compt=new Text();
		compt.setFont(Font.font ("Verdana", 20));
		GridPane.setColumnIndex(compt, 1);
		GridPane.setHalignment(compt, HPos.CENTER);
		GridPane.setRowIndex(compt, lesNoms.size()+2);
		int i=0;
		GridPane root = new GridPane();
		for (i=0;i<lesNoms.size();i++)
		{
			GridPane.setColumnIndex(colCb.get(i), 0);
			GridPane.setRowIndex(colCb.get(i), i);
			GridPane.setHalignment(colCb.get(i), HPos.RIGHT);
			
			root.getChildren().add(colCb.get(i));
			GridPane.setColumnIndex(lesNoms.get(i), 1);
			GridPane.setRowIndex(lesNoms.get(i), i);
			root.getChildren().add(lesNoms.get(i));
	
		}
		root.getChildren().add(compt);
		Label sp= new Label("");
		sp.setFont(Font.font ("Arial", 20));
		GridPane.setColumnIndex(sp, 0);
		GridPane.setRowIndex(sp, lesNoms.size());
		root.getChildren().add(sp);
		Label nb= new Label("Nombre de tour :");
		nb.setFont(Font.font ("Arial", 20));
		GridPane.setColumnIndex(nb, 0);
		GridPane.setRowIndex(nb, lesNoms.size()+1);
		GridPane.setHalignment(nb, HPos.RIGHT);
		TextField tnb= new TextField();
		GridPane.setColumnIndex(tnb, 1);
		GridPane.setRowIndex(tnb, lesNoms.size()+1);
		Button b = new Button("Lancer");
		b.setFont(Font.font ("Arial", 20));
		GridPane.setColumnIndex(b, 0);
		GridPane.setRowIndex(b, lesNoms.size()+2);
		root.getChildren().add(b);
		GridPane.setHalignment(b, HPos.RIGHT);
		b.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				// TODO Auto-generated method stub
				new Thread(){
			        public void run(){
			        	int ii=0;
			        	pr.reset();
			    		for(Stagiaire st : pr.afficheTous()) {
			    			lesNoms.get(ii).setText(st.getPrenom()+" "+st.getNom());
			    			ii++;
			    		}
			        	int nbreTour;
						 nbreTour= Integer.parseInt(tnb.getText());
						
						  for (int i=1;i<=nbreTour;i++)
						  {
							  stTemp=null;
							do { 
							  stTemp=pr.selectionHasard(i);
							} while(!(colCb.get(pr.afficheTous().lastIndexOf(stTemp)).isSelected()));
							lesNoms.get(pr.afficheTous().lastIndexOf(stTemp)).setFill(javafx.scene.paint.Color.RED);
							lesNoms.get(pr.afficheTous().lastIndexOf(stTemp)).setText(stTemp.getPrenom()+" "+stTemp.getNom()+" "+stTemp.getScore());
							compt.setText("Tour n° "+String.valueOf(i));
							try {
								Thread.sleep(500);
								lesNoms.get(pr.afficheTous().lastIndexOf(stTemp)).setFill(javafx.scene.paint.Color.BLACK);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
//						      PauseTransition pauseTransition = new PauseTransition(Duration.millis(1000));
						    //  fen.show();
						  }
						  Stagiaire st;
					      st =pr.gagnant();
						  Platform.runLater(() -> dialog = new Alert(Alert.AlertType.INFORMATION));
						  Platform.runLater(() ->dialog.setTitle("Stagiaire sélectionné"));
						  Platform.runLater(() ->dialog.setHeaderText("Voici le stagiaire sélectionné :"));
						  Platform.runLater(() ->dialog.setContentText(st.toString()));
						  Platform.runLater(() -> dialog.showAndWait());
						

						  
						 
			        }
			      }.start();
			
			}
			
		});
		root.getChildren().add(nb);
		root.getChildren().add(tnb);
		Scene sc = new Scene(root);
		fen.setScene(sc);
		
		
		
	}


}
